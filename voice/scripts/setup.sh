#!/bin/bash

apt-get update
apt install python3-pip python3-venv -y

python3 -m venv env
source env/bin/activate

git clone https://github.com/coqui-ai/TTS
cd TTS
pip install -e .
cd ..

python3 download_dataset.py
CUDA_VISIBLE_DEVICES=0 python3 train.py