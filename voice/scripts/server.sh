#!/bin/bash
set -euo pipefail

if [ $# -eq 0 ]
  then
    echo "supply command"
    exit 1
fi

if [ $# -eq 1 ]
  then
    echo "supply ip"
    exit 1
fi

# do upload
if [ "$1" = "upload" ]
  then
    ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "${2}"
    rsync -v -r train.py scripts/setup.sh scripts/download_dataset.py root@$2:~/mira/
    exit 0
fi

# do download
if [ "$1" = "download" ]
  then
    rsync -v -r --exclude 'checkpoint*' --exclude 'best_model_*' root@$2:~/mira/models ./
    exit 0
fi

echo "no valid command supplied"
exit 1