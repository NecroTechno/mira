#!/bin/bash
set -euo pipefail

if [ $# -eq 0 ]
  then
    echo "supply command"
    exit 1
fi

if [ $# -eq 1 ]
  then
    echo "supply ip"
    exit 1
fi

# do upload
if [ "$1" = "upload" ]
  then
    ssh-keygen -f "${HOME}/.ssh/known_hosts" -R "${2}"
    rsync -v -r src requirements_server.txt scripts/setup.sh data root@$2:~/mira/
    exit 0
fi

# do download
if [ "$1" = "download" ]
  then
    rm -rf tokenizer/*
    rm -rf models/*
    rsync -v -r --exclude 'checkpoint*' root@$2:~/mira/\{models,tokenizer\} ./
    exit 0
fi

echo "no valid command supplied"
exit 1