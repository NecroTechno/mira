#!/usr/bin/env python
# encoding: utf-8

import tweepy #https://github.com/tweepy/tweepy
import csv
import sys
from dotenv import dotenv_values

env = dotenv_values(".env")

#Twitter API credentials
consumer_key = env["CONSUMER_KEY"]
consumer_secret = env["CONSUMER_SECRET"]
access_key = env["ACCESS_KEY"]
access_secret = env["ACCESS_SECRET"]
bearer = env["BEARER"]


def get_all_tweets(screen_name):
    #Twitter only allows access to a users most recent 3240 tweets with this method
    
    client = tweepy.Client(
        bearer_token=bearer,
        consumer_key=consumer_key,
        consumer_secret=consumer_secret,
        access_token=access_key,
        access_token_secret=access_secret,
    )

    user_id = client.get_users(usernames=[screen_name])[0][0].id

    # retrieve first n=`max_results` tweets
    tweets = client.get_users_tweets(user_id)
    tweets_list = []
    # retrieve using pagination until no tweets left
    while True:
        if not tweets.data:
            break
        tweets_list.extend(tweets.data)
        if not tweets.meta.get('next_token'):
            break
        tweets = client.get_users_tweets(
            id=user_id,
            pagination_token=tweets.meta['next_token'],
        )

    file = open(f'raw_data/new_{screen_name}_tweets.txt', "w", encoding='utf-8', errors='replace')
    for tweet in tweets_list:
        if not tweet.text.startswith("RT"):
            file.writelines(tweet.text + "\n")
    file.close()


if __name__ == '__main__':
	#pass in the username of the account you want to download
	get_all_tweets(sys.argv[1])
