#!/bin/bash

apt-get update
apt install python3-pip -y
# apt install python3.8-venv -y
# python3 -m venv env
# source env/bin/activate
pip3 install -r requirements_server.txt -f https://download.pytorch.org/whl/torch_stable.html