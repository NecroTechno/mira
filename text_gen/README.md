# mira text gen

1. clean raw data `mira clean-raw`
2. spin up server for training
3. upload to server `./scripts/server.sh upload {ip}`
4. connect to server and run setup.sh
5. train data on server `mira train`
6. download model from server `./scripts/server.sh download {ip}`
7. generate sentences `mira generate`