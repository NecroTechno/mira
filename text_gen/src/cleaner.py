import html
import re
import string
from enum import Enum 

CLEANING_TYPES = Enum('CLEANING_TYPES', 'hnc masto lain lobsters twitter')
cleaner_data_dir = "./cleaner_data/"

class Cleaner:
    def __init__(self):
        self.HTML_CLEANR = re.compile("<.*?>")
        self.HTML_PUNCTUATION_CLEANR = re.compile("[.,;:]<.*?>")
        self.CODE_CLEANR = re.compile("<code.*>.*<\/code>")
        self.WHITESPACE_CLEANR = re.compile(" +")
        self.HYPERLINK_CLEANR = re.compile("http\S+")
        self.MENTION_CLEANR = re.compile("@.*$")
        self.CHAN_MENTION_CLEANR = re.compile(">>.([\S]+)")
        self.EMOJI_CLEANR = re.compile("\:[^)]*\:")

    def clean_text(self, txt):
        txt = "".join(v for v in txt if v not in string.punctuation.replace('-', '').replace('/', '')).lower()
        txt = txt.replace("-", " ")
        txt = txt.replace("/", " ")
        txt = txt.encode("utf8").decode("ascii", "ignore")
        txt = re.sub(self.WHITESPACE_CLEANR, " ", txt)

        slurs = open(cleaner_data_dir + "slurs.txt").readlines()
        for x in slurs:
            txt = re.sub(re.compile(x.strip()), "tryhard", txt)

        txt = txt.strip()
        return txt

    def clean_html(self, txt, type):
        txt = html.unescape(txt)
        txt = txt.replace("<br />", " ")
        txt = re.sub(self.CODE_CLEANR, " ", txt)
        if type == CLEANING_TYPES.masto:
            # txt = re.sub(self.HTML_PUNCTUATION_CLEANR, " ", txt)
            txt = txt.replace("><p", "> <p")
            txt = re.sub(self.HTML_CLEANR, "", txt)
        else:
            txt = re.sub(self.HTML_CLEANR, " ", txt)
        txt = re.sub(self.HYPERLINK_CLEANR, " ", txt)
        txt = txt.replace("\n", " ")
        return txt

    def clean_social(self, txt):
        txt = re.sub(self.MENTION_CLEANR, "", txt)
        txt = re.sub(self.EMOJI_CLEANR, "", txt)
        return txt
    
    def clean_chan(self, txt):
        txt = re.sub(self.CHAN_MENTION_CLEANR, "", txt)
        txt = re.sub(re.compile("fuar*k"), "fuck", txt)
        txt = re.sub(re.compile("soykaf"), "shit", txt)
        txt = re.sub(re.compile("lainon"), "guy", txt)
        return txt

    def clean_all(self, txt, type):
        if type == CLEANING_TYPES.hnc or type == CLEANING_TYPES.lobsters:
            txt = self.clean_html(txt, type)
            txt = self.clean_text(txt)
            return txt
        if type == CLEANING_TYPES.masto or type == CLEANING_TYPES.twitter:
            txt = self.clean_html(txt, type)
            txt = self.clean_social(txt)
            txt = self.clean_text(txt)
            return txt
        if type == CLEANING_TYPES.lain:
            txt = self.clean_html(txt, type)
            txt = self.clean_chan(txt)
            txt = self.clean_text(txt)
            return txt
