import click
import string, os
import pandas as pd
from pathlib import Path
from tokenizers import ByteLevelBPETokenizer
from tokenizers.pre_tokenizers import ByteLevel
import torch
import tensorflow as tf
from transformers import (
    AutoConfig,
    GPT2Config,
    TFGPT2LMHeadModel,
    GPT2LMHeadModel,
    GPT2Tokenizer,
    AdamW
)
from transformers import LineByLineTextDataset
from transformers import DataCollatorForLanguageModeling
from transformers import Trainer, TrainingArguments
from transformers import pipeline
from transformers import WEIGHTS_NAME, CONFIG_NAME
from datasets import load_dataset
import json
import random

from cleaner import Cleaner, CLEANING_TYPES

context_length = 512
tokenizer_data_path = "./tokenizer/"
model_data_path = "./models/"


@click.command()
def clean_raw():
    CLEANING_HNC = False
    CLEANING_MASTO = True
    CLEANING_LAIN = False
    CLEANING_LOBSTERS = False
    CLEANING_TWITTER = True

    cleaning_dir = "./raw_data/"

    if CLEANING_HNC:
        # IMPORT DATA
        all_hnc = []
        for filename in os.listdir(cleaning_dir):
            if "hacker_news_comments" in filename:
                hnc_df = pd.read_csv(cleaning_dir + filename)
                all_hnc.extend(list(hnc_df.comment_text.values))
                break

        # CLEAN DATA
        cleaner = Cleaner()
        hnc_corpus = [cleaner.clean_all(x, CLEANING_TYPES.hnc) for x in random.sample(all_hnc, 2000)]

        file = open("data/hnc.txt", "w")
        for items in hnc_corpus:
            file.writelines(items + "\n")
        file.close()

    if CLEANING_MASTO:
        all_masto = []
        for filename in os.listdir(cleaning_dir):
            if "outbox_" in filename:
                posts_json = json.load(open(cleaning_dir + filename, "r", encoding="utf8"))
                for item in posts_json["orderedItems"]:
                    if isinstance(item["object"], dict):
                        if item["actor"] == "https://mastodon.social/users/NecroTechno":
                            if (
                                item["object"]["type"] == "Note"
                                and "https://ghost.cafe/users/NecroTechno"
                                not in item["object"]["to"]
                            ):
                                all_masto.append(item["object"]["content"])
                        elif item["actor"] == "https://ghost.cafe/users/NecroTechno":
                            if item["object"]["type"] == "Note":
                                all_masto.append(item["object"]["content"])

        cleaner = Cleaner()
        masto_corpus = [cleaner.clean_all(x, CLEANING_TYPES.masto) for x in all_masto]

        while "" in masto_corpus:
            masto_corpus.remove("")

        file = open("data/masto.txt", "w")
        for items in masto_corpus:
            file.writelines(items + "\n")
        file.close()

    if CLEANING_LAIN:
        all_lain = []
        for filename in os.listdir(cleaning_dir):
            if "lain_posts" in filename:
                lp_ff = open(cleaning_dir + filename).readlines()
                all_lain.extend(lp_ff)
                break

        # CLEAN DATA
        cleaner = Cleaner()
        lain_corpus = [cleaner.clean_all(x, CLEANING_TYPES.lain) for x in random.sample(all_lain, 2000)]
        
        file = open("data/lain.txt", "w")
        for items in lain_corpus:
            if len(items) > 0:
                file.writelines(items + "\n")
        file.close()

    if CLEANING_LOBSTERS:
        all_lobsters = []
        for filename in os.listdir(cleaning_dir):
            if "lobster_comments" in filename:
                lc_ff = open(cleaning_dir + filename).readlines()
                all_lobsters.extend(lc_ff)
                break

        # CLEAN DATA
        cleaner = Cleaner()
        lobster_corpus = [cleaner.clean_all(x, CLEANING_TYPES.lobsters) for x in random.sample(all_lobsters, 3000)]
        
        file = open("data/lobsters.txt", "w")
        for items in lobster_corpus:
            if len(items) > 0:
                file.writelines(items + "\n")
        file.close()

    if CLEANING_TWITTER:
        all_twitter = []
        for filename in os.listdir(cleaning_dir):
            if "tweets" in filename:
                tweets = open(cleaning_dir + filename, errors='replace').readlines()
                all_twitter.extend(tweets)

        cleaner = Cleaner()
        twitter_corpus = [cleaner.clean_all(x, CLEANING_TYPES.twitter) for x in all_twitter]

        file = open("data/twitter.txt", "w")
        for items in twitter_corpus:
            if len(items) > 0:
                file.writelines(items + "\n")
        file.close()

    return

# editable command for easy testing
@click.command()
def workbook():
    print(torch.cuda.is_available())
    return


@click.command()
def train():
    torch.cuda.empty_cache()

    tokenizer = GPT2Tokenizer.from_pretrained(
        "gpt2", bos_token="<s>", eos_token="</s>", pad_token="<pad>"
    )

    config = AutoConfig.from_pretrained("gpt2")
    model = GPT2LMHeadModel.from_pretrained("gpt2", config=config)
    model.resize_token_embeddings(len(tokenizer))

    data_collator = DataCollatorForLanguageModeling(tokenizer, mlm=False)

    dataset = load_dataset(
        "text", data_files=[
            # "./data/hnc.txt",
            "./data/masto.txt",
            # "./data/lain.txt",
            # "./data/lobsters.txt",
            "./data/twitter.txt"
            ]
    )

    dataset.shuffle()

    dataset = dataset["train"].train_test_split(0.3)

    train_dataset = dataset["train"]
    eval_dataset = dataset["test"]

    def encode(examples):
        return tokenizer(
            examples["text"],
            truncation=True,
            max_length=context_length,
            padding="max_length",
        )

    train_dataset = train_dataset.map(encode, batched=True, batch_size=100)
    eval_dataset = eval_dataset.map(encode, batched=True, batch_size=100)
    train_dataset.set_format(type="torch", columns=["input_ids", "attention_mask"])
    eval_dataset.set_format(type="torch", columns=["input_ids", "attention_mask"])

    args = TrainingArguments(
        output_dir=model_data_path,
        overwrite_output_dir=True,
        num_train_epochs=7,
        logging_steps=100,
        save_steps=1000,
        save_strategy="steps",
        per_device_train_batch_size=8,
        per_device_eval_batch_size=8,
        gradient_accumulation_steps=8,
        learning_rate = 5e-4,
        warmup_steps = 1e2,
        adam_epsilon = 1e-8,
        weight_decay=0.0,
        push_to_hub=False,
        evaluation_strategy="steps",
        resume_from_checkpoint=True,
    )

    trainer = Trainer(
        model=model,
        tokenizer=tokenizer,
        args=args,
        data_collator=data_collator,
        train_dataset=train_dataset,
        eval_dataset=eval_dataset,
    )

    trainer.train()
    trainer.save_model(model_data_path)
    tokenizer.save_pretrained(tokenizer_data_path)
    return


@click.command()
# @click.argument('seed_text')
@click.option('--seed', '-s', default=None)
def generate(seed):
    tokenizer = GPT2Tokenizer.from_pretrained(tokenizer_data_path)
    model = GPT2LMHeadModel.from_pretrained(model_data_path)
    text = "<s>"  # encoding the input text
    if seed is not None:
        text = seed

    # pipe = pipeline("text-generation", model=model, tokenizer=tokenizer)
    # print(pipe(text, num_return_sequences=5))

    encoded_prompt = tokenizer.encode(text, add_special_tokens=False, return_tensors="pt")

    output_sequences = model.generate(
        input_ids=encoded_prompt,
        top_k=50, 
        max_length = 50,
        top_p=0.92,
        temperature=0.90,
        repetition_penalty=1.2,
        do_sample=True,
        num_return_sequences=10,
    )

    for index, seq in enumerate(output_sequences):
        output = tokenizer.decode(seq.squeeze(), skip_special_tokens=True)
        print("Prediction " + str(index) + ":\n" + output.strip() + "\n")
    return


@click.group(help="MIRA COMMAND CENTER")
def cli():
    pass


cli.add_command(workbook)
cli.add_command(clean_raw)
cli.add_command(train)
cli.add_command(generate)

if __name__ == "__main__":
    cli()
